# A Production Server

So you're ready for the big leagues of devops for Machine Learning.
Google colab is great for sharing a bit of code you're working on.
But eventually you'll want to "graduate" to using a production server.
This can help prepare you for the day you need to collaborate with professionals.
It can also build good habits and skills to start using version control and automated testing every day,
This tutorial will show you how to escape the sandbox ([vendor lock-in](https://www.cloudflare.com/learning/cloud/what-is-vendor-lock-in/)) of Google Colab so you can run your machine learning models on a real server.

## Create an SSH Key

If you've set up your GitLab account with an SSH key, you can reuse that same key for the rest of this tutorial and you can skip this section.

Otherwise, creating a key is straight forward.
You only need to run one command, `ssh-keygen` from your terminal (console).
But before you do that, let's make sure you don't already have an SSH key.
Usually SSH keys are stored in your user directory in a hidden directory (starts with a dot) such as: `$HOME/.ssh/*`.

So list the files in your SSH directory with a command like this:

```console
$ ls -al ~/.ssh

drwx------  2 hobs hobs 4096 Mar 12 12:44 .
drwxr-xr-x 67 hobs hobs 4096 Mar 27 21:51 ..
-rw-------  1 hobs hobs 1879 Feb 17 16:14 config
-rw-------  1 hobs hobs 1679 Aug 19  2020 id_rsa
-rw-r--r--  1 hobs hobs  404 Aug 19  2020 id_rsa.pub
-rw-r--r--  1 hobs hobs 9084 Feb 12 20:57 known_hosts

$ tree ~/.ssh

.ssh
├── config
├── id_rsa
├── id_rsa.pub
├── known_hosts
...
```

The private half of your key is stored in `~/.ssh/id_rsa`.
The public half of your key is stored in `~/.ssh/id_rsa.pub`.
You'll learn about the `config` and `hosts` files later.
All of these files are just text files so use `cat` or your favorite text editor to see what they look like.

If you don't have any `id_rsa` files listed, or you get an error because you don't yet have a `.ssh` directory, don't panic, `ssh-keygen` will create all that for you.

Don't forget to accept all the default settings by hitting the [ENTER] key after each of the 3 questions that pops up.
Read each of the questions so you understand a bit more about how SSH works.
You are leaving the SSH key password blank, so you can interact with the ssh server without ever having to enter a password again (except to log into your laptop):


```console
$ ssh-keygen

Generating public/private rsa key pair.
Enter file in which to save the key (/home/hobs/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/hobs/.ssh/id_rsa
Your public key has been saved in /home/hobs/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:K6me6JiK8NeQ+KVUQmkzx4SypZW5yxJ/4zOnob2fki0 hobs@tangibleai-laptop
The key's randomart image is:
+---[RSA 3072]----+
|     B.          |
|  . % o          |
|   O =           |
|  + o .          |
|   = =  S        |
|  o B +. .       |
|.  + B++.        |
|o+ .+=Eoo.       |
|*.+o= +Xo        |
+----[SHA256]-----+
```

Now you can rerun your `ls` or `tree` commands to see the files that command created.

## Logging In

Make sure you have all the information you need to log into an ssh server.
You need 3 things:

1. An IP address or domain for the server, called the host name:

SERVER_HOSTNAME='132.239.93.250'

2. The user name you are using to log into the server:

SERVER_USERNAME='hobs'

3. The password you use to log into the server:

SERVER_PASSWORD='server-administrator-will-give-you-this'

There's actually a 4th thing that you'll need in rare situations, the port number.
The default port number for SSH is 22.
If your server administrator changes this, she'll tell you about it.

Here's how you use that info to log into a server:

```console
# ssh hobs@132.239.93.250
$ ssh $SERVER_USERNAME@$SERVER_HOSTNAME
hobs@132.239.93.250's password:
```

If you successfully enter the password at the prompt you should get a message like this in your terminal:

```console
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-135-generic x86_64)
...
```

Once you are able to successfully log into the server you can log out with the exit command:

```console
$ exit
```

```console
logout
Connection to 132.239.93.250 closed.
```

That will bring you back to your laptop so you can set up passworless login using your ssh key.

## Passwordless Login

This is what my `~/.config` file looks like:

#### *`~/.config`*
```
Host ucsdgpu
    HostName 132.239.93.250
    User hobs
    IdentityFile ~/.ssh/id_rsa
    # secret-password-here-as-a-comment-if-you-forget
```

You want to name the server (host) something short, such as `ucsdgpu` so you don't have to remember that IP address.
Notice that you can indicate where your ssh-key is with that IdentityFile configuration.

One last thing remains, this is the last time you'll have to enter the password for the server:

```console
$ ssh-copy-id ucsdgpu
```

```console
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/hobs/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
```

That should be it. Now test it out:

```console
$ ssh ucsdgpu
```

```console
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-135-generic x86_64)
...
```

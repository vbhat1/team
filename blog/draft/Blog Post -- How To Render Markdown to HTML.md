# Blog Post -- How To Render Markdown to HTML

To display URLs and code snippets formatted in markdown within Slack messages and web pages for Tanbot we need to find some python that can render markdown to html.
That way the intern onboarding bot (Eddy replacement) can have his messages in the database stored in human-readable markdown while rendering them in browser-readable format (HTML).
User Story: user wants to be able to click on links in messages from Tanbot so they can quickly finish a learning assignment and check it off their todo list.

Looks like there's a `django-markdown` package and perhaps just a separarate `markdown` package.
I haven't found any Django Template `tags` or `filters` to do this automatically within a Django Template.
We can just use the `safe` Django Template filter to let the template renderer know that we'll be passing in rendered HTML from the backend.

Here's a blog post by somone else with a similar problem: https://hakibenita.com/django-markdown#converting-markdown-to-html

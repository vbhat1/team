# Datasets

For machine learning you just want a *tall* tabular dataset, one that has many more rows than columns. 
You also would like to know a little something about kinds of records (rows) in the dataset.
Is each row a person? a place? a product? 
And you would like to be able to imagine ways you might convert most of the values to numbers.
Machine learning is just math.
So you need to convert a table of strings and dates and addresses to numbers before you can get started.

Look for a column that would be interesting to predict (the target variable) if you know all or some of the other columns (the feature variables).
Don't pay attention to how others used the data for machine learning or statistical modeling. 
Your idea may be better.
And it will certainly be more fun that what someone else dreamed up.

## Machine Learning Datasets

- [fsu.edu CSV file format examples](https://people.sc.fsu.edu/~jburkardt/data/csv/): small CS course datasets by Jack Burkardt
- [Stanford GloVe word vectors](https://nlp.stanford.edu/projects/glove/)
- [pd.read_html on Wikipedia tables](wikipedia.org)
- [paperswithcode.com datasets](https://paperswithcode.com/datasets): state of the art benchmark 
- [fsu.edu DS datasets](https://people.sc.fsu.edu/~jburkardt/datasets/datasets.html): small machine learning and computer science by Jack Burkardt
- [Google N-grams](https://storage.googleapis.com/books/ngrams/books/datasetsv3.html) Billions of 1-5-grams from books (Gutenberg), lags 1 yr behind search engine (2020-02 data available in 2021-02)
- [Kaggle.com](kaggle.com)
- [TREC datasets](https://trec.nist.gov/data/qamain.html): Text Retrieval conference benchmark datasets, including QA (reading comp)
- [Data.gov](data.gov)

## Big Data DB Formats

- [column stores](https://towardsdatascience.com/guide-to-file-formats-for-machine-learning-columnar-training-inferencing-and-the-feature-store-2e0c3d18d4f9)

